// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

/*! 
 *  
 *   \page licence Licensing
 *    
 *     libgp - Gaussian process library for Machine Learning
 *
 *      \verbinclude "../COPYING"
 */

#ifndef __GP_H__
#define __GP_H__

#define _USE_MATH_DEFINES
#include <cmath>
#include <Eigen/Dense>

#include "cov.h"
#include "sampleset.h"

namespace libgp {
  
  /** Gaussian process regression.
   *  @author Manuel Blum */
  class GaussianProcess
  {
  public:
    
    /** Create and instance of GaussianProcess with given input dimensionality 
     *  and covariance function. */
    GaussianProcess (size_t input_dim, std::string covf_def);
    
    /** Create and instance of GaussianProcess from file. */
    GaussianProcess (const char * filename);
    
    virtual ~GaussianProcess ();
    
    /** Write current gp model to file. */
    void write(const char * filename);
    
    /** Predict target value for given input.
     *  @param x input vector
     *  @return predicted value */
    virtual double f(const double x[]);
    virtual double f_approx(const double x[]);
    
    /** Calculate gradient of mean-function f.
     *  @param x input vector
     *  @param grad gradient of f at x */
    virtual void grad_f(const double x[], Eigen::VectorXd &grad);
    virtual void grad_f_approx(const double x[], Eigen::VectorXd &grad);
    
    /** Predict variance of prediction for given input.
     *  @param x input vector
     *  @return predicted variance */
    virtual double var(const double x[]);
    virtual void grad_var(const double x[], Eigen::VectorXd &grad);
    virtual double var_approx(const double x[]);
    virtual void grad_var_approx(const double x[], Eigen::VectorXd &grad);
    /** Add input-output-pair to sample set.
     *  Add a copy of the given input-output-pair to sample set.
     *  @param x input array
     *  @param y output value
     */
    void add_pattern(const double x[], double y);
    void set_ind(const Eigen::MatrixXd &x);

    bool set_y(size_t i, double y);

    /** Get number of samples in the training set. */
    size_t get_sampleset_size();
    
    /** Clear sample set and free memory. */
    void clear_sampleset();
    
    /** Get reference on currently used covariance function. */
    CovarianceFunction & covf();
    
    /** Get input vector dimensionality. */
    size_t get_input_dim();

    double log_likelihood();
    
    Eigen::VectorXd log_likelihood_gradient();



    /** The training sample set. */
    SampleSet * sampleset;

  protected:
    
    /** The covariance function of this Gaussian process. */
    CovarianceFunction * cf;
    
    /** The training sample set. */
   // SampleSet * sampleset;
    
    /** Alpha is cached for performance. */ 
    Eigen::VectorXd alpha;
    Eigen::VectorXd alpha_ind;
    
    /** Last test kernel vector. */
    Eigen::VectorXd k_star;
    Eigen::VectorXd k_star_ind;

    Eigen::VectorXd ind_row;

    /** Linear solver used to invert the covariance matrix. */
//    Eigen::LLT<Eigen::MatrixXd> solver;
    bool inducing;
    Eigen::MatrixXd L;
    Eigen::MatrixXd L_ind;
    Eigen::MatrixXd L_mn;
    Eigen::MatrixXd L_mm;
    Eigen::MatrixXd L_mm_star;
    Eigen::MatrixXd ind;
    size_t max_samples;
    bool l_mm_build;
    Eigen::LDLT<Eigen::MatrixXd> L_ind_solver;
    double cond_epsilon;
    

    /** Input vector dimensionality. */
    size_t input_dim;
    
    /** Update test input and cache kernel vector. */
    void update_k_star(const Eigen::VectorXd &x_star);
    void update_k_star_ind(const Eigen::VectorXd &x_star);

    void update_alpha();
    void update_alpha_ind();

    /** Compute covariance matrix and perform cholesky decomposition. */
    virtual void compute();
    
    bool alpha_needs_update;
    bool alpha_ind_needs_update;

    // temporaries
    Eigen::MatrixXd JT;
    Eigen::VectorXd tmp_grad;
    

  };
}

#endif /* __GP_H__ */
