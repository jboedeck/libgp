// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

#include "cov_sum.h"
#include "cmath"

namespace libgp
{
  
  CovSum::CovSum()
  {
  }
  
  CovSum::~CovSum()
  {
    delete first;
    delete second;
  }
  
  bool CovSum::init(int n, CovarianceFunction * first, CovarianceFunction * second)
  {
    this->input_dim = n;
    this->first = first;
    this->second = second;
    param_dim_first = first->get_param_dim();
    param_dim_second = second->get_param_dim();
    param_dim = param_dim_first + param_dim_second;
    loghyper.resize(param_dim);
    loghyper.setZero();
    return true;
  }
  
  double CovSum::get(const Eigen::VectorXd &x1, const Eigen::VectorXd &x2)
  {
    return first->get(x1, x2) + second->get(x1, x2);
  }
  
  void CovSum::grad(const Eigen::VectorXd &x1, const Eigen::VectorXd &x2, Eigen::VectorXd &grad)
  {
    Eigen::VectorXd grad_first(param_dim_first);
    Eigen::VectorXd grad_second(param_dim_second);
    first->grad(x1, x2, grad_first);
    second->grad(x1, x2, grad_second);
    grad.head(param_dim_first) = grad_first;
    grad.tail(param_dim_second) = grad_second;
  }
  
  void CovSum::grad_input(const Eigen::VectorXd &x1, const Eigen::VectorXd &x2, Eigen::VectorXd &grad)
  {
    grad.resize(input_dim);
    Eigen::VectorXd grad_first, grad_second;
    first->grad_input(x1, x2, grad_first);
    second->grad_input(x1, x2, grad_second);
    grad = grad_first + grad_second;
  }

  void CovSum::hessian_input(const Eigen::VectorXd &x1, const Eigen::VectorXd &x2, Eigen::MatrixXd &H)
  {
	H.resize(input_dim, input_dim);
	Eigen::MatrixXd H_first, H_second;
	first->hessian_input(x1, x2, H_first);
	second->hessian_input(x1, x2, H_second);
	H = H_first + H_second;
  }

  void CovSum::set_loghyper(const Eigen::VectorXd &p)
  {
    CovarianceFunction::set_loghyper(p);
    first->set_loghyper(p.head(param_dim_first));
    second->set_loghyper(p.tail(param_dim_second));
  }
  
  std::string CovSum::to_string()
  {
    return "CovSum("+first->to_string()+", "+second->to_string()+")";
  }
}
