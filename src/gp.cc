// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

#include "gp.h"
#include "cov_factory.h"
#include "cov_sum.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <ctime>

namespace libgp {
  
  const double log2pi = log(2*M_PI);
  const double initial_L_size = 2000;

  GaussianProcess::GaussianProcess (size_t input_dim, std::string covf_def)
  {
    // set input dimensionality
    this->input_dim = input_dim;
    // create covariance function
    // TODO we might have to adapt this conditioning 
    cond_epsilon = 1e-4;
    CovFactory factory;
    cf = factory.create(input_dim, covf_def);
    cf->loghyper_changed = 0;
    sampleset = new SampleSet(input_dim);
    L.resize(initial_L_size, initial_L_size);
    inducing = false;
  }
  
  GaussianProcess::GaussianProcess (const char * filename) 
  {
    int stage = 0;
    std::ifstream infile;
    double y;
    infile.open(filename);
    std::string s;
    double * x = NULL;
    inducing = false;
    // TODO we might have to adapt this conditioning 
    cond_epsilon = 1e-4;
    L.resize(initial_L_size, initial_L_size);
    while (infile.good()) {
      getline(infile, s);
      // ignore empty lines and comments
      if (s.length() != 0 && s.at(0) != '#') {
        std::stringstream ss(s);
        if (stage > 2) {
          ss >> y;
          for(size_t j = 0; j < input_dim; ++j) {
            ss >> x[j];
          }
          add_pattern(x, y);
        } else if (stage == 0) {
          ss >> input_dim;
          sampleset = new SampleSet(input_dim);
          x = new double[input_dim];
        } else if (stage == 1) {
          CovFactory factory;
          cf = factory.create(input_dim, s);
          cf->loghyper_changed = 0;
        } else if (stage == 2) {
          Eigen::VectorXd params(cf->get_param_dim());
          for (size_t j = 0; j<cf->get_param_dim(); ++j) {
            ss >> params[j];
          }
          cf->set_loghyper(params);
        }
        stage++;
      }
    }
    infile.close();
    if (stage < 3) {
      std::cerr << "fatal error while reading " << filename << std::endl;
      exit(EXIT_FAILURE);
    }
    delete [] x;
  }
  
  GaussianProcess::~GaussianProcess ()
  {
    // free memory
    delete sampleset;
    delete cf;
  }  

  void GaussianProcess::set_ind(const Eigen::MatrixXd &x) 
  {
    ind = x;
    inducing = true;
    ind_row.resize(x.cols()); // only resize once as long as num inducing points DON'T change
    l_mm_build = false;
    // JTS this is a HACK, we set loghyper to changed
    // so that compute will be executed at least once
    // and the inducing points can be used safely
    cf->loghyper_changed = true;
    max_samples = ind.rows();
    L_mn.setZero(max_samples, initial_L_size);
    L_mm.setZero(max_samples, max_samples);        
    L_mm_star.setZero(max_samples, max_samples);
  }

  double GaussianProcess::f(const double x[])
  {
    if (sampleset->empty()) return 0;
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha();
    update_k_star(x_star);
    return k_star.dot(alpha);    
  }

  double GaussianProcess::f_approx(const double x[])
  {
    if (sampleset->empty()) return 0;
    if (!inducing || sampleset->size() <= max_samples) return f(x);
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha_ind();
    update_k_star_ind(x_star);
    return k_star_ind.dot(alpha_ind);    
  }

  
  void GaussianProcess::grad_f(const double x[], Eigen::VectorXd &grad)
  {
    if (sampleset->empty()) {
        grad = Eigen::VectorXd::Zero(input_dim);
        return;
    }
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    
    // compute alpha = C^(-1) * y
    compute(); 
    update_alpha();
    
    // compute J^T
    int n = sampleset->size();
    //Eigen::MatrixXd JT = Eigen::MatrixXd::Zero(input_dim, n);
    //Eigen::VectorXd tmp_grad = Eigen::VectorXd::Zero(input_dim);
    for (int i=0; i<n; ++i) {
        cf->grad_input(x_star, sampleset->x(i), tmp_grad);
        JT.col(i) = tmp_grad;
    }
    
    grad = JT * alpha;
  }

  void GaussianProcess::grad_f_approx(const double x[], Eigen::VectorXd &grad)
  {
    if (!inducing || sampleset->size() <= max_samples) return grad_f(x, grad);
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    
    // compute alpha = C^(-1) * y
    compute(); 
    update_alpha_ind();
    
    // compute J^T
    int m = ind.rows();
    JT.resize(input_dim, m);
    tmp_grad.resize(input_dim);
    for (int i=0; i<m; ++i) {
      ind_row = ind.row(i);
      cf->grad_input(x_star, ind_row, tmp_grad);
      JT.col(i) = tmp_grad;
    }
    
    grad = JT * alpha_ind;
  }
  
  double GaussianProcess::var(const double x[])
  {
    if (sampleset->empty()) return 0;
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha();
    update_k_star(x_star);
    int n = sampleset->size();
    Eigen::VectorXd v = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(k_star);
    return cf->get(x_star, x_star) - v.dot(v);	
  }

  void GaussianProcess::grad_var(const double x[], Eigen::VectorXd &grad)
  {
    if (sampleset->empty()) {
        grad = Eigen::VectorXd::Zero(input_dim);
        return;
    }
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha();
    update_k_star(x_star);
    int n = sampleset->size();
    Eigen::VectorXd v = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(k_star);
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().adjoint().solveInPlace(v);
    Eigen::MatrixXd JT = Eigen::MatrixXd::Zero(input_dim, n);
    Eigen::VectorXd tmp_grad = Eigen::VectorXd::Zero(input_dim);
    for (int i=0; i<n; ++i) {
        cf->grad_input(x_star, sampleset->x(i), tmp_grad);
        JT.col(i) = tmp_grad;
    }
    cf->grad_input(x_star, x_star, tmp_grad);
    grad = tmp_grad - JT * 2. * v;
  }

  double GaussianProcess::var_approx(const double x[])
  {
    if (sampleset->empty()) return 0;
    if (!inducing || sampleset->size() < max_samples) return var(x);
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha_ind();
    update_k_star_ind(x_star);
    Eigen::VectorXd v = L_ind_solver.solve(k_star_ind);
    //assert(cf->to_string().substr(0,6).compare("CovSum") == 0);
    CovSum *cfs = (CovSum *) cf;
    return cfs->second->get(sampleset->x(0), sampleset->x(0)) * k_star_ind.dot(v);	
  }

  void GaussianProcess::grad_var_approx(const double x[], Eigen::VectorXd &grad)
  {
    if (!inducing || sampleset->size() < max_samples) return grad_var(x, grad);
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha_ind();
    update_k_star_ind(x_star);
    int m = ind.rows();
    Eigen::VectorXd v = L_ind_solver.solve(k_star_ind);
    //assert(cf->to_string().substr(0,6).compare("CovSum") == 0);
    CovSum *cfs = (CovSum *) cf;
    v = cfs->second->get(sampleset->x(0), sampleset->x(0)) *  2. * v;
    //Eigen::MatrixXd JT = Eigen::MatrixXd::Zero(input_dim, m);
    //Eigen::VectorXd tmp_grad = Eigen::VectorXd::Zero(input_dim);
    JT.resize(input_dim, m);
    tmp_grad.resize(input_dim);
    for (int i=0; i<m; ++i) {
        cf->grad_input(x_star, ind.row(i), tmp_grad);
        JT.col(i) = tmp_grad;
    }
    grad = JT * v;
  }

  void GaussianProcess::compute()
  {
    // can previously computed values be used?
    if (!cf->loghyper_changed) return;
    cf->loghyper_changed = false;
    int n = sampleset->size();
    // resize L if necessary
    if (n > L.rows()) L.resize(n + initial_L_size, n + initial_L_size);
    // compute kernel matrix (lower triangle)
    for(size_t i = 0; i < sampleset->size(); ++i) {
      for(size_t j = 0; j <= i; ++j) {
        L(i, j) = cf->get(sampleset->x(i), sampleset->x(j));
      }
    }
    // also build kernel matrix of inducing points
    if (sampleset->size() > max_samples && inducing && ind.rows() > 0) {
        //assert(cf->to_string().substr(0,6).compare("CovSum") == 0);
        CovSum *cfs = (CovSum *) cf;
        int m = ind.rows();
        if (n > L_mn.cols()) {
            L_mn.resize(m, n + initial_L_size);
        }
        for(int i = 0; i < ind.rows(); ++i) {
            for(size_t j = 0; j < sampleset->size(); ++j) {
                L_mn(i, j) = cfs->first->get(ind.row(i), sampleset->x(j));
            }
        }
        if (! l_mm_build) {
            for(int i = 0; i < ind.rows(); ++i) {
                for(int j = 0; j < ind.rows(); ++j) {
                    L_mm(i, j) = cfs->first->get(ind.row(i), ind.row(j));
                }
            }
            l_mm_build = true;
        }
        L_mm_star = L_mn.topLeftCorner(m, n) * L_mn.topLeftCorner(m, n).transpose();
        L_ind = L_mm_star + cfs->second->get(sampleset->x(0), sampleset->x(0)) * L_mm.topLeftCorner(m, m);
        //L_ind.topLeftCorner(m, m) = L_ind.topLeftCorner(m, m).selfadjointView<Eigen::Lower>().llt().matrixL();
        L_ind += Eigen::MatrixXd::Identity(m,m) * cond_epsilon;
        L_ind_solver = L_ind.ldlt();
        alpha_ind_needs_update = true;
    }

    // perform cholesky factorization
    //solver.compute(K.selfadjointView<Eigen::Lower>());
    L.topLeftCorner(n, n) = L.topLeftCorner(n, n).selfadjointView<Eigen::Lower>().llt().matrixL();
    alpha_needs_update = true;
  }
  
  void GaussianProcess::update_k_star(const Eigen::VectorXd &x_star)
  {
    k_star.resize(sampleset->size());
    for(size_t i = 0; i < sampleset->size(); ++i) {
      k_star(i) = cf->get(x_star, sampleset->x(i));
    }
  }

  void GaussianProcess::update_k_star_ind(const Eigen::VectorXd &x_star)
  {
    if (inducing && ind.rows() > 0) {
        int m = ind.rows();	
	k_star_ind.resize(m);	 
        for(int i = 0; i < m; ++i) {
	  ind_row = ind.row(i);
	  k_star_ind(i) = cf->get(x_star, ind_row);
        }
    }
  }

  void GaussianProcess::update_alpha_ind()
  {
    // can previously computed values be used?
    // JTS TODO
    if (!alpha_ind_needs_update) return;
    alpha_ind_needs_update = false;
    // Map target values to VectorXd
    const std::vector<double>& targets = sampleset->y();
    Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
    if (sampleset->size() > max_samples && inducing && ind.rows() > 0) {
        int m = ind.rows();
        int n = y.size();
        Eigen::VectorXd y_proj = L_mn.topLeftCorner(m,n) * y;
        alpha_ind = L_ind_solver.solve(y_proj);
        //alpha_ind = L_ind_solver.solve(y_proj);
        //alpha_ind = L_ind.topLeftCorner(m, m).triangularView<Eigen::Lower>().solve(y_proj);
        //L_ind.topLeftCorner(m, m).triangularView<Eigen::Lower>().adjoint().solveInPlace(alpha_ind);
    }
  }

  void GaussianProcess::update_alpha()
  {
    // can previously computed values be used?
    if (!alpha_needs_update) return;
    alpha_needs_update = false;
    alpha.resize(sampleset->size());
    // Map target values to VectorXd
    const std::vector<double>& targets = sampleset->y();
    Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
    int n = sampleset->size();
    alpha = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(y);
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().adjoint().solveInPlace(alpha);
    if (sampleset->size() > max_samples && inducing && ind.rows() > 0) {
        int m = ind.rows();
        Eigen::VectorXd y_proj = L_mn.topLeftCorner(m, n) * y;
        alpha_ind = L_ind_solver.solve(y_proj);
    }
  }
  
  void GaussianProcess::add_pattern(const double x[], double y)
  {
    int n = sampleset->size();
    sampleset->add(x, y);
    // create kernel matrix if sampleset is empty
    if (n == 0) {
      L(0,0) = sqrt(cf->get(sampleset->x(0), sampleset->x(0)));
      cf->loghyper_changed = false;
    // recompute kernel matrix if necessary
    } else if (cf->loghyper_changed) {
      compute();
    // update kernel matrix 
    } else {
      Eigen::VectorXd k(n);
      for (int i = 0; i<n; ++i) {
        k(i) = cf->get(sampleset->x(i), sampleset->x(n));
      }
      double kappa = cf->get(sampleset->x(n), sampleset->x(n));
      // resize L if necessary
      if (sampleset->size() > (size_t)L.rows()) {
        L.conservativeResize(n + initial_L_size, n + initial_L_size);
      }
      L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(k);
      L.block(n,0,1,n) = k.transpose();
      // set the last entry, we add a small number to avoid negative values 
      // due to floating point inaccuracies if kappa == k.dot(k)
      L(n,n) = sqrt(fabs(kappa - k.dot(k)));
      // compute update to L_mn as well
      if (inducing && ind.rows() > 0) {
        int m = ind.rows();
        Eigen::VectorXd k_m(m);
        // JTS: This is a dirty hack remove it at some point in time
        //assert(cf->to_string().substr(0,6).compare("CovSum") == 0);
        CovSum *cfs = (CovSum *) cf;
        for (int i = 0; i<m; ++i) {
            k_m(i) = cfs->first->get(ind.row(i), sampleset->x(n));
            //k_m(i) = cf->get(ind.row(i), sampleset->x(n));
        }
        if (n > L_mn.cols()) {
            L_mn.conservativeResize(m, n + initial_L_size);
        }
        L_mn.col(n).head(m) = k_m;
        if (! l_mm_build) {
            for(int i = 0; i < m; ++i) {
                for(int j = 0; j < m; ++j) {
                    L_mm(i, j) = cfs->first->get(ind.row(i), ind.row(j));
                    //L_mm(i, j) = cf->get(ind.row(i), ind.row(j));
                }
            }
            l_mm_build = true;
        }
        //L_mm_star += k_m * k_m.transpose();
        L_mm_star = L_mn.topLeftCorner(m,n) * L_mn.topLeftCorner(m,n).transpose();
        L_ind = L_mm_star + cfs->second->get(sampleset->x(0), sampleset->x(0)) * L_mm.topLeftCorner(m, m);      
        L_ind += Eigen::MatrixXd::Identity(m,m) * cond_epsilon;
        //L_ind.topLeftCorner(m, m) = L_ind.topLeftCorner(m, m).selfadjointView<Eigen::Lower>().llt().matrixL();
        L_ind_solver = L_ind.ldlt();
        //L_ind_solver = L_ind.colPivHouseholderQr();
        alpha_ind_needs_update = true;
      }
    }
    alpha_needs_update = true;
  }

  bool GaussianProcess::set_y(size_t i, double y) 
  {
    if(sampleset->set_y(i,y)) {
      alpha_needs_update = true;
      alpha_ind_needs_update = true;
      return 1;
    }
    return false;
  }

  size_t GaussianProcess::get_sampleset_size()
  {
    return sampleset->size();
  }
  
  void GaussianProcess::clear_sampleset()
  {
    sampleset->clear();
  }
  
  void GaussianProcess::write(const char * filename)
  {
    // output
    std::ofstream outfile;
    outfile.open(filename);
    time_t curtime = time(0);
    tm now=*localtime(&curtime);
    char dest[BUFSIZ]= {0};
    strftime(dest, sizeof(dest)-1, "%c", &now);
    outfile << "# " << dest << std::endl << std::endl
    << "# input dimensionality" << std::endl << input_dim << std::endl 
    << std::endl << "# covariance function" << std::endl 
    << cf->to_string() << std::endl << std::endl
    << "# log-hyperparameter" << std::endl;
    Eigen::VectorXd param = cf->get_loghyper();
    for (size_t i = 0; i< cf->get_param_dim(); i++) {
      outfile << std::setprecision(10) << param(i) << " ";
    }
    outfile << std::endl << std::endl 
    << "# data (target value in first column)" << std::endl;
    for (size_t i=0; i<sampleset->size(); ++i) {
      outfile << std::setprecision(10) << sampleset->y(i) << " ";
      for(size_t j = 0; j < input_dim; ++j) {
        outfile << std::setprecision(10) << sampleset->x(i)(j) << " ";
      }
      outfile << std::endl;
    }
    outfile.close();
  }
  
  CovarianceFunction & GaussianProcess::covf()
  {
    return *cf;
  }
  
  size_t GaussianProcess::get_input_dim()
  {
    return input_dim;
  }

  double GaussianProcess::log_likelihood()
  {
    compute();
    update_alpha();
    int n = sampleset->size();
    const std::vector<double>& targets = sampleset->y();
    Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
    double det = 2 * L.diagonal().head(n).array().log().sum();
    return -0.5*y.dot(alpha) - 0.5*det - 0.5*n*log2pi;
  }

  Eigen::VectorXd GaussianProcess::log_likelihood_gradient() 
  {
    compute();
    update_alpha();
    size_t n = sampleset->size();
    Eigen::VectorXd grad = Eigen::VectorXd::Zero(cf->get_param_dim());
    Eigen::VectorXd g(grad.size());
    Eigen::MatrixXd W = Eigen::MatrixXd::Identity(n, n);

    // compute kernel matrix inverse
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(W);
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().transpose().solveInPlace(W);

    W = alpha * alpha.transpose() - W;

    for(size_t i = 0; i < n; ++i) {
      for(size_t j = 0; j <= i; ++j) {
        cf->grad(sampleset->x(i), sampleset->x(j), g);
        if (i==j) grad += W(i,j) * g * 0.5;
        else      grad += W(i,j) * g;
      }
    }

    return grad;
  }
}
