// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

#include "gp.h"
#include "gp_utils.h"

#include <Eigen/Dense>

using namespace libgp;

int main (int argc, char const *argv[])
{
  int n=200, m=100;
  double tss = 0, tss_approx = 0, error, f, y, f_approx;
  // initialize Gaussian process for 2-D input using the squared exponential 
  // covariance function with additive white noise.
  GaussianProcess gp(2, "CovSum ( CovSEiso, CovNoise)");
  // initialize hyper parameter vector
  Eigen::VectorXd params(gp.covf().get_param_dim());
  params << 0.0, 0.0, -2.0;
  // set parameters of covariance function
  gp.covf().set_loghyper(params);
  Eigen::MatrixXd ind(50, 2);
  ind.setRandom();
  ind = ind * 2;
  gp.set_ind(ind);
  // add training patterns
  for(int i = 0; i < n; ++i) {
      //std::cout << "adding pattern " << i << std::endl;
    double x[] = {drand48()*4-2, drand48()*4-2};
    y = Utils::hill(x[0], x[1]) + Utils::randn() * 0.1;
    gp.add_pattern(x, y);
  }
  // total squared error
  Eigen::VectorXd g1(2);
  Eigen::VectorXd g2(2);
  for(int i = 0; i < m; ++i) {
    double x[] = {drand48()*4-2, drand48()*4-2};
    f = gp.f(x);
    f_approx = gp.f_approx(x);
    gp.grad_f(x, g1);
    gp.grad_f_approx(x, g2);
    //std::cout << "f: " << f << " f_approx: " << f_approx << std::endl;
    //std::cout << "g_f: " << g1.transpose() << " g_f_approx: " << g2.transpose() << std::endl;
    y = Utils::hill(x[0], x[1]);
    error = f - y;
    tss += error*error;
    error = f_approx - y;
    tss_approx += error*error;
  }
  std::cout << "mse = " << tss/m << std::endl;
  std::cout << "mse_approx = " << tss_approx/m << std::endl;
	return EXIT_SUCCESS;
}
